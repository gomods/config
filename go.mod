module gitee.com/gomods/config

go 1.15

require (
	github.com/kardianos/osext v0.0.0-20190222173326-2bc1f35cddc0
	github.com/spf13/cast v1.3.1
	github.com/unknwon/goconfig v0.0.0-20200908083735-df7de6a44db8
	gopkg.in/yaml.v2 v2.4.0
)
