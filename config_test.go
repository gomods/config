package config

import (
	"os"
	"testing"
)

func TestGetConf(t *testing.T) {
	dir, _ := os.Getwd()
	SetConfigPath(dir + "/config/app.yaml")

	if GetConf("goconfig", "hosts") == "127.0.0.1 127.0.0.2 127.0.0.3" {
		t.Log("pass")
	} else {
		t.Error("fail")
	}

	if len(GetConfArr("goconfig", "hosts")) == 3 {
		t.Log("pass")
	} else {
		t.Error("fail")
	}

	if GetConfStringMap("goyamlStringMap")["name"] == "goyaml" {
		t.Log("pass")
	} else {
		t.Error("fail")
	}
}
